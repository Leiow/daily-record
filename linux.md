# Linux 命令

## 字符集转换

1. unicode 转 utf-8

> iconv -f gbk -t utf-8 index.html > index2.html

其中-f指的是原始文件编码，-t是输出编码 index.html 是原始文件 index2.html是输出结果文件

2. 查看字符集

> iconv -l

## 清楚 BOM

> dos2unix some_file