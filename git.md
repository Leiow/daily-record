# git 相关

## git 拉取指定分支

1. git fetch origin dev
2. git checkout -b dev origin/dev

## git pull 冲突时放弃本地修改，使用远程代码强制覆盖本地代码

> 1. git fetch --all // 只下载代码到本地，不进行合并操作
> 2. git reset --hard origin/master // 把 HEAD 指向到最新下载的版本