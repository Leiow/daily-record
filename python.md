# python 相关

## 解决无法引入模块的错误

项目启动的时候，会遇到无法引入某个模块的问题，不管是第三方模块还是自己开发的模块。问题是由于 python 的环境变量设置错误的原因。

在遇到此类问题的时候，需要先看下环境变量中是否有 `PYTHONPATH`，即在终端输出 `export` 输出所有的环境变量，或者使用 `export | grep PYTHONPATH` 直接输出 python 的环境变量。如果没有或者冰没有指向当前的项目，那么就无法引入模块。

解决办法：

> export PYTHONPATH=/project/path

比如项目在 `/home/data/python-project` 这个目录，那么上述表达式应为 `export PYTHONPATH=/home/data/python-project`。

## python 环境使用过程中注意点

### virtualenv 部分参数说明

- 构建并使用虚拟环境

  ```shell
  # -q 不显示详细信息
  # -v 显示详细信息
  # --clear 清空非 root 用户，并重头开始创建隔离环境
  # --no-site-packates 令隔离环境不能访问系统全局的site-packages目录
  virtualenv --clear --no-site-packages env
  source env/bin/activate
  ```

  - virtualenv 参数

    | 参数 | 说明 |
    | -- | -- |
    | -q, --quiet | 不显示详细信息 |
    | -v, --verbose | 显示详细信息 |
    | --clear | 清空非 root 用户的安装，并重头开始创建隔离环境 |
    | --no-site-packages | 令隔离环境不能访问系统全局的 site-packages 目录 |
    | --system-site-packages | 令隔离环境可以访问系统全局的 site-packages 目录 |

- 检查环境变量

  ```shell
  export | grep PYTHONPATH
  # 如果环境变量没有指向当前项目目录，则需要执行下面指令
  export PYTHONPATH="/data/medical_stream/"
  ```

- 安装项目依赖包

  ```shell
  pip install -r requirements.txt
  ```

## pypy 安装

系统 CentOS 6.9，从 pypy.org 下载的 [Source (tar.bz2)](`https://bitbucket.org/pypy/pypy/downloads/pypy2-v6.0.0-src.tar.bz2`) 安装包，在解压后，会提示如下错误：

> pypy: error while loading shared libraries: libbz2.so.1.0: cannot open shared object file: No such file or directory

为了避免这个问题，使用 portable 的包进行安装，本次使用的是 [pypy-6.0.0-linux_x86_64-portable.tar.bz2](https://bitbucket.org/squeaky/portable-pypy/downloads/pypy-6.0.0-linux_x86_64-portable.tar.bz2)

可能会使用到的依赖和安装步骤：

1. yum install -y zlib-devel bzip2-devel openssl-devel xz-libs wget xz gcc tar gcc-c++
2. cd /usr/local/src/
3. wget https://bitbucket.org/squeaky/portable-pypy/downloads/pypy-6.0.0-linux_x86_64-portable.tar.bz2
4. tar xf pypy-6.0.0-linux_x86_64-portable.tar.bz2
5. cd pypy-6.0.0-linux_x86_64-portable/bin/
6. mv pypy-6.0.0-linux_x86_64-portable /usr/local/lib/pypy
7. ln -s /usr/local/lib/pypy/bin/pypy /usr/bin/pypy
8. pypy -m ensurepip
9. ln -s /usr/local/lib/pypy/bin/pip /usr/bin/pip
10. pip install -U pip wheel

## docopt 注意事项

1. 在 `doc` 中的每一行，以 `-` 或 `--` 开头的，都被视为选项描述；
2. 使用两个空格将选项和描述隔开

## filter(func, list)

`filter` 函数是使用 func 函数将 list 进行过滤。

`medical_stream/tasks/base/inpatient_layer_2/BC/worker_mapping.py` 中有一行代码是

```python
src_val_lst = filter(None, src_val_lst)
```

这行代码是为了去除参数中的 None、False、空字符串等为 False 的值。

## dict  list

### Dict

1. dict.get(key, default=None)
2. dict.items()
3. dict.keys()
4. dict.values()
5. dict.update(dict)

### List

1. list.append(obj)
2. list.extend(seq)
3. list.insert(index, obj)
4. list.pop([index=-1])
5. list.remove(obj)
