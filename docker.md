# docker 相关

## docker 用法

- `docker images` 获取镜像信息
  - `-a` 列出所有镜像
- `docker tag source:tag dest:tag` 给镜像添加标签
- `docker inspect image-name:tag` 查看详细信息
- `docker history` 查看镜像历史
  - 使用 `--no-trunc` 输出完整命令
- `docker search` 搜索镜像
  - 使用 `-s` 或 `--stars=X` 指定仅显示评价为指定星级以上的镜像
- `docker rmi`  删除镜像
  - 该命令后加标签，若标签为多个，则只删除标签，不会删除镜像；若标签只有一个，才会删除镜像；
  - 该命令后加 ID，会先尝试删除所有指向该镜像的标签，之后删除镜像文件本身；
  - 加上 `-f` 参数，为强制删除镜像
- 创建镜像
  - `docker commit [OPTIONS] CONTAINER [REPOSITORY[:TAG]]` 基于已有镜像的容器创建
    - `-a`, `--author=""` 作者信息
    - `-m`, `--message=""` 提交消息
    - `-p`, `--pause=true` 提交时暂停容器运行
  - `docker import [OPTIONS] file|URL|- [REPOSITORY[:TAG]]`
- `docker save` 存出镜像
  - 如将本地 `ubuntu:latest` 镜像存出，则应为 `docker save -o myubuntu_14.04.tar ubuntu:latest`
- `docker load` 载入镜像
  - `docker load --input ubuntu_14.04.tar`
  - `docker load < ubuntu_14.04.tar`
- `docker push` 上传镜像
  - `docker push NAME[:TAG] | [REGISTRY_HOST[:REGISTRY_PORT]/]NAME[:TAG]`

### 操作 Docker 容器

- `docker create` 创建容器，如 `docker create -it ubuntu:latest`
- `docker start` 启动容器，如 `docker start ID`
- `docker run` 新建并启动容器，如 `docker run ubuntu /bin/bash 'hello world'`
  - 使用 `-it` 参数，可以打开一个伪终端，并保持标准输入常开，如 `docker run -it ubuntu`；
  - 使用 `-d` 参数，可以在后台常驻容器，如 `docker run -d ubuntu`
- `docker stop` 终止容器
- `docker exec` 进入容器，如 `docker exec -it ID /bin/bash`

## Docker 镜像上传步骤

1. 编写 `Dockerfile`；
2. 构建镜像 `docker build -t IMAGE-NAME[:TAG] DOCKFILE-PATH`；
3. 给镜像增加标签 `docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]`；
4. 上传到 docker-hub `docker push NAME[:TAG]`

## Docker build

1. `-t` 指定标签
2. `-f` 用于指定 Dockerfile 路径，另外环境目录参数不可省略，否则会报上下文错误，比如 `docker build -t project:dev -f /soft/Dockerfile /Docker`
