# Kafka 操作

kafka 的服务运行于 `master` 服务器中

## 查看 topic

> kafka-topics --list --zookeeper master:2181/kafka

## 查看 topic 详细信息

> kafka-topics --zookeeper master:2181/kafka --describe --topic rd_dw_xxx

## 创建 topic

> kafka-topics --zookeeper master:2181/kafka --create --replication-factor 1 --partitions 5 --topic test

## 删除 topic

> kafka-topics --zookeeper master:2181/kafka --delete --topic rd_dw_xxx

## 启动消费者

> kafka-console-consumer --zookeeper master:2181/kafka --from-beginning --topic rd_dw_followup_layer_3_normal

## 启动生产者

> kafka-console-producer --broker-list master:9092 --topic rd_dw_followup_layer_3_normal

## Get number of messages in a topic

> bin/kafka-run-class.sh kafka.tools.GetOffsetShell --broker-list localhost:9092 --topic mytopic --time -1 --offsets 1 | awk -F  ":" '{sum += $3} END {print sum}'

## Get the earliest offset still in a topic

> bin/kafka-run-class.sh kafka.tools.GetOffsetShell --broker-list localhost:9092 --topic mytopic --time -2

## Get the latest offset still in a topic

> bin/kafka-run-class.sh kafka.tools.GetOffsetShell --broker-list localhost:9092 --topic mytopic --time -1

## Consume messages with the console consumer

> bin/kafka-console-consumer.sh --new-consumer --bootstrap-server localhost:9092 --topic mytopic --from-beginning

## Get the consumer offsets for a topic

> bin/kafka-consumer-offset-checker.sh --zookeeper=localhost:2181 --topic=mytopic --group=my_consumer_group

## Kafka Consumer Groups

### List the consumer groups known to Kafka

> bin/kafka-consumer-groups.sh --zookeeper localhost:2181 --list  (old api)
> bin/kafka-consumer-groups.sh --new-consumer --bootstrap-server localhost:9092 --list (new api)

### View the details of a consumer group

> bin/kafka-consumer-groups.sh --zookeeper localhost:2181 --describe --group \<name\>

## kafkacat

### Getting the last five message of a topic

> kafkacat -C -b localhost:9092 -t mytopic -p 0 -o -5 -e

## Zookeeper

### Starting the Zookeeper Shell

> bin/zookeeper-shell.sh localhost:2181