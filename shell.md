# Shell

## 关于 `BRE` 和 `ERE`

- grep
    最早的文本匹配程序，使用 POSIX 定义的基本正则表达式（Basic Regular Expression, BRE）。
- egrep
    扩展式 grep（Extended grep）。这个程序使用扩展正则表达式（Extended Regular Expression，ERE）。
- fgrep
    快速 grep（Fast grep）。这个版本匹配固定字符串而非正则表达式，它使用优化的算法，能更有效地匹配固定字符串。

## 非空字符串的判断

```shell
#!/bin/bash
# 注意各个字符之间的空格，是必须的

param=""
if [ ! -n "$param" ]; then
    echo "空"
else
    echo "非空"
fi
```

```shell
#!/bin/bash

param=""
if ["$param"x == x ]; then
    echo "空"
else
    echo "非空"
fi
```
